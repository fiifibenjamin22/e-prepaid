//
//  chargesTableCells.swift
//  E-Prepaid
//
//  Created by AKWESI on 8/7/18.
//  Copyright © 2018 AiR-Technologies-Ltd. All rights reserved.
//

import UIKit

class chargesTableCell: UITableViewCell {
    
    let chargesLbl = uicomponents.universalLbl(name: AllStrings.chargesApplied, txtColor: .lightGray, textFontSize: 16)
    let amountLbl = uicomponents.universalLbl(name: AllStrings.amountPaid, txtColor: .lightGray, textFontSize: 16)
    let percentageLbl = uicomponents.universalLbl(name: AllStrings.pecentage, txtColor: .lightGray, textFontSize: 16)
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: UITableViewCellStyle.default, reuseIdentifier: AllStrings.chargesCellId)
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
