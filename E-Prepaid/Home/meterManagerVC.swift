//
//  meterManagerVC.swift
//  E-Prepaid
//
//  Created by AKWESI on 8/7/18.
//  Copyright © 2018 AiR-Technologies-Ltd. All rights reserved.
//

import UIKit

class meterManagerVC: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.tableView.register(meterManagerCell.self, forCellReuseIdentifier: AllStrings.meterManagerCellId)
        self.tableView.separatorStyle = .none
        self.tableView.tableFooterView = UIView()
        self.setupUI()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AllStrings.meterManagerCellId, for: indexPath) as! meterManagerCell
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
