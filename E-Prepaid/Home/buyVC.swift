//
//  homeVC.swift
//  E-Prepaid
//
//  Created by AKWESI on 8/7/18.
//  Copyright © 2018 AiR-Technologies-Ltd. All rights reserved.
//

import UIKit

class buyVC: UIViewController {
    
    let scroll = uicomponents.scrollView()
    let meterIDField = uicomponents.textFields(placeholder: AllStrings.meterId)
    let amountField = uicomponents.textFields(placeholder: AllStrings.amount)
    let ownerField = uicomponents.textFields(placeholder: AllStrings.owner)
    let locationField = uicomponents.textFields(placeholder: AllStrings.location)
    let saveSwitchLabel = uicomponents.universalLbl(name: AllStrings.saveMeter, txtColor: .lightGray, textFontSize: 16)
    let saveSwitch = uicomponents.saveMeterSwitch()
    let buyBtn = uicomponents.pressBtn(title: AllStrings.BuyTitle, backColor: .primaryColor, textColor: .white)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.saveSwitch.addTarget(self, action: #selector(saveMeter), for: .touchUpInside)
        self.setupUI()
        self.setupNavItems()
    }
    
    func setupNavItems() {
        let leftItem = UIBarButtonItem(title: "Menu", style: .plain, target: self, action: #selector(handleMore))
        navigationItem.leftBarButtonItems = [leftItem]
    }
    
    @objc func handleMore() {
        print("Open Menu")
    }
    
    @objc func saveMeter() {
        if saveSwitch.isOn {
            self.saveSwitch.thumbTintColor = .white
        }else{
            self.saveSwitch.thumbTintColor = .primaryColor
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
        
}
