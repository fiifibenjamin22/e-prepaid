//
//  registerVC.swift
//  E-Prepaid
//
//  Created by AKWESI on 8/7/18.
//  Copyright © 2018 AiR-Technologies-Ltd. All rights reserved.
//

import UIKit

class registerVC: BaseScrollViewController {
    
    let customnavBar = uicomponents.universalView(viewColor: .secondaryColor)
    
    let profileImage = uicomponents.universalImage(imageName: "profile-placeholder")
    let nameField = uicomponents.textFields(placeholder: AllStrings.nameHolder)
    let phoneNumber = uicomponents.textFields(placeholder: AllStrings.phoneNumberHolder)
    let passwordField = uicomponents.textFields(placeholder: AllStrings.passwordHolder)
    let confirmPassword = uicomponents.textFields(placeholder: AllStrings.confirmPassword)
    let registerBtn = uicomponents.pressBtn(title: AllStrings.registerBtnTitle, backColor: .secondaryColor, textColor: .primaryColor)
    let haveAccount = uicomponents.universalLbl(name: AllStrings.alreadyHaveAnAccount, txtColor: .white, textFontSize: 11)
    let leftSeperator = uicomponents.universalView(viewColor: .white)
    let rightSeperator = uicomponents.universalView(viewColor: .white)
    let loginBtn = uicomponents.pressBtn(title: AllStrings.loginBtnTitle, backColor: .secondaryColor, textColor: .primaryColor)
    
    let lineSeperator = uicomponents.universalView(viewColor: .secondaryColor)
    let customtabBar = uicomponents.universalView(viewColor: .tetiaryColor)
    
    var placeHolder = NSMutableAttributedString()
    var namePlaceHolder = AllStrings.nameHolder
    var phoneHolder = AllStrings.phoneNumberHolder
    var passwordHolder = AllStrings.passwordHolder
    var confirmPassHolder = AllStrings.confirmPassword
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .primaryColor
        self.loginBtn.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        self.registerBtn.addTarget(self, action: #selector(handleRegister), for: .touchUpInside)
        
        placeHolder = NSMutableAttributedString(string: namePlaceHolder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)])
        self.nameField.attributedPlaceholder = placeHolder
        
        placeHolder = NSMutableAttributedString(string: phoneHolder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)])
        self.phoneNumber.attributedPlaceholder = placeHolder
        
        placeHolder = NSMutableAttributedString(string: passwordHolder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)])
        self.passwordField.attributedPlaceholder = placeHolder
        
        placeHolder = NSMutableAttributedString(string: confirmPassHolder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)])
        self.confirmPassword.attributedPlaceholder = placeHolder
        
        self.setupUI()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func handleLogin() {
        let vc = loginVC()
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func handleRegister() {
        let vc = tabBarVC()
        self.present(vc, animated: true, completion: nil)
    }
}
