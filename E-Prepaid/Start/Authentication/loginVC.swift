//
//  ViewController.swift
//  E-Prepaid
//
//  Created by AKWESI on 8/6/18.
//  Copyright © 2018 AiR-Technologies-Ltd. All rights reserved.
//

import UIKit

class loginVC: UIViewController {
    
    let scrollView = uicomponents.scrollView()
    let phoneNumber = uicomponents.textFields(placeholder: AllStrings.phoneNumberHolder)
    let passwordField = uicomponents.textFields(placeholder: AllStrings.passwordHolder)
    let loginBtn = uicomponents.pressBtn(title: AllStrings.loginBtnTitle, backColor: .btnPrimaryColor, textColor: .white)
    let orLbl = uicomponents.universalLbl(name: AllStrings.orLablelTitle, txtColor: .white, textFontSize: 11)
    let leftSeperator = uicomponents.universalView(viewColor: .white)
    let rightSeperator = uicomponents.universalView(viewColor: .white)
    let registerBtn = uicomponents.pressBtn(title: AllStrings.registerBtnTitle, backColor: .btnPrimaryColor, textColor: .white)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .primaryColor
        self.loginBtn.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        self.registerBtn.addTarget(self, action: #selector(handleRegister), for: .touchUpInside)
        self.setupUI()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func handleLogin() {
        let vc = tabBarVC()
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func handleRegister() {
        let vc = registerVC()
        self.present(vc, animated: true, completion: nil)
    }
}

