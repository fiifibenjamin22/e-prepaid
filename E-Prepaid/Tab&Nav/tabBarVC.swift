//
//  tabBarVC.swift
//  E-Prepaid
//
//  Created by AKWESI on 8/7/18.
//  Copyright © 2018 AiR-Technologies-Ltd. All rights reserved.
//

import UIKit

class tabBarVC: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.barTintColor = .primaryColor
        self.tabBar.tintColor = .white
        
//        guard let images = UIImage(named: "") else { return }
        
        viewControllers = [
            generateNavController(for: buyVC(), title: AllStrings.BuyTitle),
            generateNavController(for: chargesVC(), title: AllStrings.chargesTitle),
            generateNavController(for: transactionsVC(), title: AllStrings.transactionTitle),
            generateNavController(for: meterManagerVC(), title: AllStrings.managerTitle),
        ]
    }
    
    fileprivate func generateNavController(for rootViewController: UIViewController, title: String) -> UIViewController {
        let navController = navBarVC(rootViewController: rootViewController)
        navController.tabBarItem.title = title
//        navController.tabBarItem.image = image
        rootViewController.navigationItem.title = title
        navController.navigationBar.prefersLargeTitles = true
        navController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        navController.navigationBar.largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        return navController
    }
}
