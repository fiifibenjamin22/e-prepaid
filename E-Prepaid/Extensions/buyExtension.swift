//
//  homeExtension.swift
//  E-Prepaid
//
//  Created by AKWESI on 8/7/18.
//  Copyright © 2018 AiR-Technologies-Ltd. All rights reserved.
//

import UIKit

extension buyVC {
    
    func setupUI() {
        
        self.view.addSubview(scroll)
        self.scroll.addSubview(meterIDField)
        self.scroll.addSubview(amountField)
        self.scroll.addSubview(ownerField)
        self.scroll.addSubview(locationField)
        self.scroll.addSubview(saveSwitchLabel)
        self.scroll.addSubview(saveSwitch)
        self.scroll.addSubview(buyBtn)
        
        scroll.backgroundColor = .clear
        scroll.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        scroll.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        scroll.widthAnchor.constraint(equalToConstant: self.view.frame.size.width).isActive = true
        scroll.heightAnchor.constraint(equalToConstant: self.view.frame.size.height).isActive = true

        meterIDField.borderStyle = .roundedRect
        meterIDField.topAnchor.constraint(equalTo: self.scroll.safeAreaLayoutGuide.topAnchor, constant: 100).isActive = true
        meterIDField.leadingAnchor.constraint(equalTo: self.scroll.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive = true
        meterIDField.trailingAnchor.constraint(equalTo: self.scroll.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        meterIDField.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        
        amountField.borderStyle = .roundedRect
        amountField.topAnchor.constraint(equalTo: meterIDField.bottomAnchor, constant: 12).isActive = true
        amountField.leftAnchor.constraint(equalTo: meterIDField.leftAnchor).isActive = true
        amountField.rightAnchor.constraint(equalTo: meterIDField.rightAnchor).isActive = true
        amountField.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        
        ownerField.borderStyle = .roundedRect
        ownerField.topAnchor.constraint(equalTo: amountField.bottomAnchor, constant: 12).isActive = true
        ownerField.leftAnchor.constraint(equalTo: amountField.leftAnchor).isActive = true
        ownerField.rightAnchor.constraint(equalTo: amountField.rightAnchor).isActive = true
        ownerField.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        
        locationField.borderStyle = .roundedRect
        locationField.topAnchor.constraint(equalTo: ownerField.bottomAnchor, constant: 12).isActive = true
        locationField.leftAnchor.constraint(equalTo: ownerField.leftAnchor).isActive = true
        locationField.rightAnchor.constraint(equalTo: ownerField.rightAnchor).isActive = true
        locationField.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        
        saveSwitchLabel.textAlignment = .right
        saveSwitchLabel.topAnchor.constraint(equalTo: locationField.bottomAnchor, constant: 12).isActive = true
        saveSwitchLabel.leftAnchor.constraint(equalTo: locationField.leftAnchor).isActive = true
        saveSwitchLabel.widthAnchor.constraint(equalToConstant: (self.view.frame.width / 2) - 8).isActive = true
        saveSwitchLabel.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        
        saveSwitch.topAnchor.constraint(equalTo: saveSwitchLabel.topAnchor, constant: 8).isActive = true
        saveSwitch.leftAnchor.constraint(equalTo: saveSwitchLabel.rightAnchor, constant: 8).isActive = true
        saveSwitch.rightAnchor.constraint(equalTo: locationField.rightAnchor).isActive = true
        saveSwitch.bottomAnchor.constraint(equalTo: saveSwitchLabel.bottomAnchor).isActive = true
        
        buyBtn.layer.borderWidth = 2
        buyBtn.layer.borderColor = UIColor.primaryColor.cgColor
        buyBtn.topAnchor.constraint(equalTo: saveSwitch.bottomAnchor, constant: 12).isActive = true
        buyBtn.leftAnchor.constraint(equalTo: locationField.leftAnchor, constant: 12).isActive = true
        buyBtn.rightAnchor.constraint(equalTo: locationField.rightAnchor, constant: -12).isActive = true
        buyBtn.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
    }
}
