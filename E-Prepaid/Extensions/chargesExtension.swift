//
//  chargesExtension.swift
//  E-Prepaid
//
//  Created by AKWESI on 8/7/18.
//  Copyright © 2018 AiR-Technologies-Ltd. All rights reserved.
//

import UIKit

extension chargesVC {
    
    func setupUI(){
        
    }
}

extension chargesTableCell {
    
    func setupUI(){
        
        self.addSubview(chargesLbl)
        self.addSubview(amountLbl)
        self.addSubview(percentageLbl)
        
        let width : CGFloat = self.frame.width / 2.5
        
        chargesLbl.textAlignment = .center
        chargesLbl.backgroundColor = .white
        chargesLbl.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        chargesLbl.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        chargesLbl.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        chargesLbl.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        amountLbl.textAlignment = .center
        amountLbl.textColor = .white
        amountLbl.topAnchor.constraint(equalTo: chargesLbl.topAnchor).isActive = true
        amountLbl.leftAnchor.constraint(equalTo: chargesLbl.rightAnchor).isActive = true
        amountLbl.bottomAnchor.constraint(equalTo: chargesLbl.bottomAnchor).isActive = true
        amountLbl.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        percentageLbl.textAlignment = .center
        percentageLbl.textColor = .white
        percentageLbl.topAnchor.constraint(equalTo: amountLbl.topAnchor).isActive = true
        percentageLbl.leftAnchor.constraint(equalTo: amountLbl.rightAnchor).isActive = true
        percentageLbl.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        percentageLbl.bottomAnchor.constraint(equalTo: amountLbl.bottomAnchor).isActive = true

    }
}
