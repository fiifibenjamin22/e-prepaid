//
//  startExtension.swift
//  E-Prepaid
//
//  Created by AKWESI on 8/6/18.
//  Copyright © 2018 AiR-Technologies-Ltd. All rights reserved.
//

import UIKit

extension loginVC {
    
    func setupUI(){
        
        self.view.addSubview(scrollView)
        self.scrollView.addSubview(phoneNumber)
        self.scrollView.addSubview(passwordField)
        self.scrollView.addSubview(loginBtn)
        self.scrollView.addSubview(orLbl)
        self.scrollView.addSubview(leftSeperator)
        self.scrollView.addSubview(rightSeperator)
        self.scrollView.addSubview(registerBtn)
        
        scrollView.backgroundColor = .clear
        scrollView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        scrollView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalToConstant: self.view.frame.size.width).isActive = true
        scrollView.heightAnchor.constraint(equalToConstant: self.view.frame.size.height).isActive = true
        
        phoneNumber.centerYAnchor.constraint(equalTo: self.scrollView.centerYAnchor, constant: -50).isActive = true
        phoneNumber.leadingAnchor.constraint(equalTo: self.scrollView.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive = true
        phoneNumber.trailingAnchor.constraint(equalTo: self.scrollView.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        phoneNumber.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        
        passwordField.isSecureTextEntry = true
        passwordField.topAnchor.constraint(equalTo: phoneNumber.bottomAnchor, constant: 12).isActive = true
        passwordField.leftAnchor.constraint(equalTo: phoneNumber.leftAnchor).isActive = true
        passwordField.rightAnchor.constraint(equalTo: phoneNumber.rightAnchor).isActive = true
        passwordField.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        
        loginBtn.layer.borderWidth = 2
        loginBtn.layer.borderColor = UIColor.white.cgColor
        loginBtn.topAnchor.constraint(equalTo: passwordField.bottomAnchor, constant: 12).isActive = true
        loginBtn.leftAnchor.constraint(equalTo: passwordField.leftAnchor, constant: 12).isActive = true
        loginBtn.rightAnchor.constraint(equalTo: passwordField.rightAnchor, constant: -12).isActive = true
        loginBtn.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        
        orLbl.layer.borderWidth = 0
        orLbl.layer.borderColor = UIColor.white.cgColor
        orLbl.textAlignment = .center
        orLbl.centerXAnchor.constraint(equalTo: self.scrollView.centerXAnchor).isActive = true
        orLbl.topAnchor.constraint(equalTo: self.loginBtn.bottomAnchor, constant: 8).isActive = true
        orLbl.widthAnchor.constraint(equalToConstant: 25).isActive = true
        orLbl.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        leftSeperator.leftAnchor.constraint(equalTo: loginBtn.leftAnchor).isActive = true
        leftSeperator.rightAnchor.constraint(equalTo: orLbl.leftAnchor, constant: -5).isActive = true
        leftSeperator.centerYAnchor.constraint(equalTo: orLbl.centerYAnchor).isActive = true
        leftSeperator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        rightSeperator.leftAnchor.constraint(equalTo: orLbl.rightAnchor, constant: 5).isActive = true
        rightSeperator.rightAnchor.constraint(equalTo: loginBtn.rightAnchor).isActive = true
        rightSeperator.centerYAnchor.constraint(equalTo: orLbl.centerYAnchor).isActive = true
        rightSeperator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        registerBtn.layer.borderWidth = 2
        registerBtn.layer.borderColor = UIColor.white.cgColor
        registerBtn.topAnchor.constraint(equalTo: orLbl.bottomAnchor, constant: 8).isActive = true
        registerBtn.leftAnchor.constraint(equalTo: loginBtn.leftAnchor).isActive = true
        registerBtn.rightAnchor.constraint(equalTo: loginBtn.rightAnchor).isActive = true
        registerBtn.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        registerBtn.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor).isActive = true
    }
}
