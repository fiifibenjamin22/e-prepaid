//
//  UIKitsApiExtentions.swift
//  E-Prepaid
//
//  Created by AKWESI on 8/6/18.
//  Copyright © 2018 AiR-Technologies-Ltd. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let primaryColor = UIColor(red: 215 / 255, green: 39 / 255, blue: 123 / 255, alpha: 1.0)
    static let btnPrimaryColor = UIColor(red: 215 / 255, green: 50 / 255, blue: 123 / 255, alpha: 1.0)
}
