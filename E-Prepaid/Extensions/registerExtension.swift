//
//  registerExtension.swift
//  E-Prepaid
//
//  Created by AKWESI on 8/7/18.
//  Copyright © 2018 AiR-Technologies-Ltd. All rights reserved.
//

import UIKit

extension registerVC {
    
    func setupUI(){
        
        self.view.addSubview(scrollView)
        self.scrollView.addSubview(nameField)
        self.scrollView.addSubview(phoneNumber)
        self.scrollView.addSubview(passwordField)
        self.scrollView.addSubview(confirmPassword)
        self.scrollView.addSubview(registerBtn)
        self.scrollView.addSubview(haveAccount)
        self.scrollView.addSubview(leftSeperator)
        self.scrollView.addSubview(rightSeperator)
        self.scrollView.addSubview(loginBtn)
        
        scrollView.backgroundColor = .clear
        scrollView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        scrollView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalToConstant: self.view.frame.size.width).isActive = true
        scrollView.heightAnchor.constraint(equalToConstant: self.view.frame.size.height).isActive = true
        
        nameField.centerYAnchor.constraint(equalTo: self.scrollView.centerYAnchor, constant: -100).isActive = true
        nameField.leadingAnchor.constraint(equalTo: self.scrollView.safeAreaLayoutGuide.leadingAnchor, constant: 8).isActive = true
        nameField.trailingAnchor.constraint(equalTo: self.scrollView.safeAreaLayoutGuide.trailingAnchor, constant: -8).isActive = true
        nameField.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        
        phoneNumber.topAnchor.constraint(equalTo: nameField.bottomAnchor, constant: 12).isActive = true
        phoneNumber.leftAnchor.constraint(equalTo: nameField.leftAnchor).isActive = true
        phoneNumber.rightAnchor.constraint(equalTo: nameField.rightAnchor).isActive = true
        phoneNumber.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        
        passwordField.isSecureTextEntry = true
        passwordField.topAnchor.constraint(equalTo: phoneNumber.bottomAnchor, constant: 12).isActive = true
        passwordField.leftAnchor.constraint(equalTo: phoneNumber.leftAnchor).isActive = true
        passwordField.rightAnchor.constraint(equalTo: phoneNumber.rightAnchor).isActive = true
        passwordField.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        
        confirmPassword.isSecureTextEntry = true
        confirmPassword.topAnchor.constraint(equalTo: passwordField.bottomAnchor, constant: 12).isActive = true
        confirmPassword.leftAnchor.constraint(equalTo: passwordField.leftAnchor).isActive = true
        confirmPassword.rightAnchor.constraint(equalTo: passwordField.rightAnchor).isActive = true
        confirmPassword.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        
        registerBtn.layer.borderWidth = 2
        registerBtn.layer.borderColor = UIColor.white.cgColor
        registerBtn.topAnchor.constraint(equalTo: confirmPassword.bottomAnchor, constant: 12).isActive = true
        registerBtn.leftAnchor.constraint(equalTo: confirmPassword.leftAnchor, constant: 12).isActive = true
        registerBtn.rightAnchor.constraint(equalTo: confirmPassword.rightAnchor, constant: -12).isActive = true
        registerBtn.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        
        haveAccount.layer.borderWidth = 0
        haveAccount.layer.borderColor = UIColor.white.cgColor
        haveAccount.textAlignment = .center
        haveAccount.centerXAnchor.constraint(equalTo: self.scrollView.centerXAnchor).isActive = true
        haveAccount.topAnchor.constraint(equalTo: self.registerBtn.bottomAnchor, constant: 8).isActive = true
        haveAccount.widthAnchor.constraint(equalToConstant: 150).isActive = true
        haveAccount.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        leftSeperator.leftAnchor.constraint(equalTo: registerBtn.leftAnchor).isActive = true
        leftSeperator.rightAnchor.constraint(equalTo: haveAccount.leftAnchor, constant: -5).isActive = true
        leftSeperator.centerYAnchor.constraint(equalTo: haveAccount.centerYAnchor).isActive = true
        leftSeperator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        rightSeperator.leftAnchor.constraint(equalTo: haveAccount.rightAnchor, constant: 5).isActive = true
        rightSeperator.rightAnchor.constraint(equalTo: registerBtn.rightAnchor).isActive = true
        rightSeperator.centerYAnchor.constraint(equalTo: haveAccount.centerYAnchor).isActive = true
        rightSeperator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        loginBtn.layer.borderWidth = 2
        loginBtn.layer.borderColor = UIColor.white.cgColor
        loginBtn.topAnchor.constraint(equalTo: haveAccount.bottomAnchor, constant: 8).isActive = true
        loginBtn.leftAnchor.constraint(equalTo: registerBtn.leftAnchor).isActive = true
        loginBtn.rightAnchor.constraint(equalTo: registerBtn.rightAnchor).isActive = true
        loginBtn.heightAnchor.constraint(equalToConstant: AllCalcs.uniformBtnHeight).isActive = true
        loginBtn.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor).isActive = true
        
    }
}
