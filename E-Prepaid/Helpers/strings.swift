//
//  strings.swift
//  E-Prepaid
//
//  Created by AKWESI on 8/6/18.
//  Copyright © 2018 AiR-Technologies-Ltd. All rights reserved.
//

import Foundation

class AllStrings {
    
    static let nameHolder = "Enter Your Name"
    static let phoneNumberHolder = "Enter Phone Number"
    static let passwordHolder = "Enter Password"
    static let confirmPassword = "Confirm Password"
    static let loginBtnTitle = "Login"
    static let registerBtnTitle = "Register"
    static let orLablelTitle = "OR"
    static let alreadyHaveAnAccount = "Already Have An Account"
    static let BuyTitle = "Buy"
    static let chargesTitle = "Charges"
    static let transactionTitle = "Transactions"
    static let managerTitle = "Meter Manager"
    static let meterId = "Enter Meter ID"
    static let amount = "Enter Amount"
    static let owner = "Enter Meter Owner's Name"
    static let location = "Enter your Location"
    static let saveMeter = "Save Meter"
    static let chargesCellId = "chargesCellId"
    static let meterManagerCellId = "meterCellId"
    static let chargesApplied = "Charges"
    static let amountPaid = "Amount"
    static let pecentage = "Percentage"
}
