//
//  uicomponents.swift
//  E-Prepaid
//
//  Created by AKWESI on 8/6/18.
//  Copyright © 2018 AiR-Technologies-Ltd. All rights reserved.
//

import UIKit

class uicomponents: UIView {
    
    static func universalView(viewColor: UIColor) -> UIView {
        let v = UIView()
        v.backgroundColor = viewColor
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }
    
    static func textFields(placeholder: String) -> UITextField {
        let txtField = UITextField()
        txtField.placeholder = placeholder
        txtField.backgroundColor = .white
        txtField.layer.cornerRadius = 18
        txtField.translatesAutoresizingMaskIntoConstraints = false
        return txtField
    }
    
    static func pressBtn(title: String, backColor: UIColor, textColor: UIColor) -> UIButton {
        let btn = UIButton(type: .system)
        btn.setTitle(title, for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btn.setTitleColor(textColor, for: .normal)
        btn.backgroundColor = backColor
        btn.layer.cornerRadius = 18
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }
    
    static func universalImage(imageName: String) -> UIImageView {
        let img = UIImageView()
        img.image = UIImage(named: imageName)
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }
    
    static func universalLbl(name: String, txtColor: UIColor, textFontSize: CGFloat) -> UILabel {
        let lbl = UILabel()
        lbl.text = name
        lbl.textColor = txtColor
        lbl.font = UIFont.boldSystemFont(ofSize: textFontSize)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }
    
    static func contentView() -> UIView {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }
    
    static func scrollView() -> UIScrollView {
        let scroll = UIScrollView()
        scroll.isScrollEnabled = true
        scroll.alwaysBounceVertical = true
        scroll.canCancelContentTouches = true
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }
    
    static func saveMeterSwitch() -> UISwitch {
        let save = UISwitch()
        save.isOn = false
        save.onTintColor = .primaryColor
        save.thumbTintColor = .primaryColor
        save.translatesAutoresizingMaskIntoConstraints = false
        return save
    }
}
